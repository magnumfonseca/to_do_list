class TasksController < ApplicationController
  def index
    @tasks = Task.where("(user_id = ? OR public = ?) AND finished = ?", current_user.id, true, false)
  end

  def new
    @task = Task.new(user_id: current_user.id)
  end

  def create
    @task = Task.new(task_params)
    if @task.save
      render action: "index"
    else
      render action: "new"
    end
  end

  def edit
    @task =Task.find(params[:id])
  end

  def update
    @task = Task.find(get_id_from_params)

    respond_to do |format|
      if @task.update_attributes(task_params)
        format.html {render action: "index"}
        format.json {render :json => @task, status: :ok}
      else
        format.html {render action: "edit"}
        format.json {render :json => @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @task = Task.find(get_id_from_params)
    @task.destroy

    respond_to do |format|
      format.html {render action: "index"}
      format.json {render :json => @task, status: :ok}
    end
  end

  private
  def get_id_from_params
    params[:id].to_i != 0 ? params[:id] : params[:task][:id]
  end

  def task_params
    params.require(:task).permit(
      :name, :description, :public, :user_id, :finished
    )
  end
end
