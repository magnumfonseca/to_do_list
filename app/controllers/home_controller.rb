class HomeController < ApplicationController
  def index
    if user_signed_in?
      @my_todo_list = Task.where(:user_id => current_user.id)
    end
  end
end
