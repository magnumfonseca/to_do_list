class SubtasksController < ApplicationController
  def new
    @subtask = Subtasks.new(task_id: params[:task_id])
  end

  def edit
    @subtask = Subtask.find(params[:id])
  end

  def create
    @subtask = Subtask.new(subtask_params)
    respond_to do |format|
      if @subtask.save
        format.html {redirect_to :back}
        format.json {render :json => @subtask, status: :ok}
      else
        format.html {render action: "edit"}
        format.json {render :json => @subtask.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    @subtask = Subtask.find(get_id_from_params)

    respond_to do |format|
      if @subtask.update_attributes(subtask_params)
        format.html {redirect_to :back}
        format.json {render json: @subtask, status: "success"}
      else
        format.html {render action: "edit"}
        format.json {render json: @subtask.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @subtask = Subtask.find(get_id_from_params)
    @subtask.destroy
    
    respond_to do |format|
      format.html {redirect_to :back}
      format.json {render json: @subtask, status: "success"}
    end
  end

  private
  def get_id_from_params
    params[:id].to_i != 0 ? params[:id] : params[:subtask][:id]
  end

  def subtask_params
    params.require(:subtask).permit(
      :name, :description, :task_id, :finished
    )
  end
end
