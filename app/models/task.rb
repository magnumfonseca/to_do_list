class Task < ActiveRecord::Base
  belongs_to :user
  has_many :subtasks, :dependent => :delete_all
  before_save :default_values
  validates :name, :presence => {message: "Task name can't be blank"}

  private
  def default_values
    self.finished ||= false
    true
  end
end
