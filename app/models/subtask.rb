class Subtask < ActiveRecord::Base
  belongs_to :task
  validates :name, :presence => {message: "Subtask name can't be blank"}
  before_save :default_values 

  private
  def default_values
    self.finished ||= false;
    true
  end
end
