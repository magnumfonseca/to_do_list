function taskDone(taskId, value) {
	$.ajax({
		type: "PUT",
		url: "tasks/update",
		dataType : 'json',
		data: {
			task: {
				id: taskId,
				finished: !value
			}
		},
		success: function() {
		}
	});
}

function subtaskDone(subtaskId, value) {
	$.ajax({
		type: "PUT",
		url: "subtasks/update",
		dataType : 'json',
		data: {
			subtask: {
				id: subtaskId,
				finished: !value
			}
		},
		success: function() {
		}
	});

}

function destroyTask(taskId, element) {
	$.ajax({
		type: "DELETE",
		url: "tasks/destroy",
		data: {
			task: {
				id: taskId
			}
		},
		success: function() {
			$(element).closest('li').hide();
		},
		dataType : 'json'
	});
}

function createNewLi(subtask) {
	var newLi = $("<li></li>")
		.append(
			"<div class='row' style='border: solid 1px white;'>" +
				"<div class='span4'>" +
					"<a lass='subnavtab' data-toggle='tab'>"+ subtask.name +
					"</a>"+
				"</div>"+
				"<div class='span5'>" +
				//"<input type='checkbox' name='finished' id='finished' value='false' onclick='"+subtaskDone(subtask.id, false)+"'>"+
				"</div></div>"
			);
	return newLi;
}

function appendNewSubtask(subtask, element) {
	var li = $('#newSubtask').parent('li');
	$('#newSubtask').remove();
	var ul = li.children('ul');
	if(ul.length) {
		ul.append(createNewLi(subtask));
	} else {
		li.append("<ul class='nav nav-tabs nav-stacked in collapse' id='1'></ul>")
		li.children('ul').append(createNewLi(subtask));
	}

}

function saveSubtask(taskId) {
	var element=$('#save');
	var name = $('#subTaskName').val();
	if (!name) return;
		$.ajax({
			type: "POST",
			url: "subtasks",
			data: {
				subtask: {
					task_id: taskId,
					name: name
				}
			},
			dataType : 'json',
			success: function(response) {
				appendNewSubtask(response, element);
			}
		});
}

function appendForm(taskId, element) {
	if ($('#newSubtask').length) {
		$('#newSubtask').remove();
		return;
	}
	$(element).closest('li').append(
			"<ul id='newSubtask' class='nav nav-tabs nav-stacked'>" +
				"<li>"+
					"<div class='row'>" +
					"<div class='span3'>" +
					"<input type='text' id='subTaskName' placeholder='Subtask name here'></input>"+
					"</div>"+
					"<div class='span1'>" +
						"<button class='btn-mini btn-success' id='save' onClick='saveSubtask("+taskId+")'>Save</button>"+
					"</div>"+
					"</div>"+
				"</li>"+
			"</ul>"
	);
}
