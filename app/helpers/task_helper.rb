module TaskHelper
  def toggle(simple_task)
    simple_task ? "tab" : "collapse"
  end
end
