Ruby Coding Test
Story Phrase
As a person with lot of tasks to do, in order to better organize myself, I want to create an

application using ruby on rails to manage the tasks and subtasks.



Business Narrative / Scenario
Develop a ruby application that has a login page and a to­do list. Each to­do inside the list is a task. All tasks created by the logged user can be marked either as public or private. If the task is private, then only the user can see the task; if it is public, then other users that log on the to­do list app will be able to see, but not to change it. Each one of the tasks can have multiple sub­tasks and the sub­tasks are always shown according to the parent task (public or private).



Functional / Acceptance Criteria
General

On login, use Devise;
To create a sub­task, you must use AJAX. It’s your choice to make a good flow that will not overload the user.


Automated Test

You must create unit tests for all Models and Helpers;
Test at least two features using RSpec + Capybara.
Design

The HTML and CSS must be used semantically (you will be evaluated for the good practices);
Use ERb or another template engine on your views.
Technical Details

You need to create a private BitBucket(bitbucket.org) repository to commit your code.
You can use the following tools to develop your app:
Git
Ruby: use the latest and stable version
Ruby on Rails: ­ use the latest and stable version
jQuery
RSpec
Capybara: https://github.com/jnicklas/capybara
SimpleForm: ­ https://github.com/plataformatec/simple_form
Devise:­ https://github.com/plataformatec/devise
Responders: ­ http://github.com/plataformatec/responders
Twitter: bootstrap (optional) ­ http://twitter.github.com/bootstrap/


Delivery
You have 3 days after we send you this task.
Once you have finished, commit your app/code to your user on bitbucket.
Give read access to the user ac-recruitment (DO NOT FORGET) to access your code.


Obs: Any doubt, send an email to ac.brazil.recruitment@avenuecode.com
