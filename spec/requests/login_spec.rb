require 'spec_helper'


feature 'User logs in' do
  let(:email) {'teste@teste.com'}
  let(:pass) {'12345678'}
  scenario 'whith valid email and password' do
    creater_user_with email, pass
    sign_in_with email, pass
    expect(page).to have_content('Logout')
  end
end

def creater_user_with(email, pass)
  User.new(email: email, password: pass).save
end

def sign_in_with(email, password)
  visit new_user_session_path
  fill_in 'Email', with: email
  fill_in 'Password', with: password
  click_button 'Log in'
end
