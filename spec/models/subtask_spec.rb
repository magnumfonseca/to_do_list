require 'rails_helper'

describe "Subtask" do
  it "should create a new subtask" do
    task = Task.new(name: "Do a Test")
    expect(task).to be_valid
    task.save
    subtask = Subtask.new(task_id: task.id, name: "do it!")
    expect(subtask).to be_valid
    expect(subtask.save).to be_truthy
  end
  it "should not save without a name" do
    task = Task.new(name: "Do a Test")
    task.valid?
    task.save
    subtask = Subtask.new(task_id: task.id)
    subtask.valid?
    expect(subtask.errors[:name]).to include("Subtask name can't be blank")
  end
end
