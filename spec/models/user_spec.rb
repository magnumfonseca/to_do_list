require 'spec_helper'

describe "User" do
  it "should create a new user" do
    user = User.new(email: "teste@teste.com", password: "12345678")
    expect(user).to be_valid
  end
  it "should not save a user without password" do
    user =  User.new(email: "teste@teste.com.br")
    user.valid?
    expect(user.errors[:password]).to include("can't be blank")
  end
  it "should not save a user without email" do
    user =  User.new(password: "098765544321")
    user.valid?
    expect(user.errors[:email]).to include("can't be blank")
  end

end
