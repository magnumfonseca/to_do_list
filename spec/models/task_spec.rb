require 'rails_helper'

describe "Task" do
  it "should create a new task" do
    task = Task.new(name: "Do a Test")
    expect(task).to be_valid
    expect(task.save).to be_truthy
  end
  it "should not save without a name" do
    task = Task.new
    task.valid?
    expect(task.errors[:name]).to include("Task name can't be blank")
  end
end
